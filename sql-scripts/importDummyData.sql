use peopleDatabase;

load data local infile '/docker-entrypoint-initdb.d/dummyData.csv' into table people fields terminated by ',' lines terminated by '\n' (full_name, address);
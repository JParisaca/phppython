<html>

<head>
	<title>Sample PHP-Python-MySQL</title>
</head>

<body>
	<h2>Sample of Products:</h2>
	<ul>
		<?php
			$json = file_get_contents('http://product-service');
			$obj = json_decode($json);

			$products = $obj->products;
			foreach ($products as $products) {
				echo "<li>$products</li>";	
			}


		?>
	</ul>
	<h2>MySQL Database connection</h2>
		<?php
		// Figure this out later
			$dbh = new PDO('mysql:host=mysql;port=3306;dbname=peopleDatabase', 'admin', 'admin');

			$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    $dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

		    var_dump($dbh->query('SELECT full_name from people'));

		    $tableList = array();
	        $result = $dbh->query("SELECT full_name FROM people;");
	        while ($row = $result->fetch(PDO::FETCH_NUM)) {
	            $tableList[] = $row[0];
	        }
	        print_r($tableList);
		?>
		</body>
</html>